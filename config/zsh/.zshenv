# .zshenv: executed by zsh for all shells
# vim: set ft=zsh:

umask 022

# set XDG env vars
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_BIN_HOME="$HOME/.local/bin"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"

# ensure XDG dirs exist
dirs=(
   $XDG_CONFIG_HOME
   $XDG_BIN_HOME
   $XDG_DATA_HOME
   $XDG_STATE_HOME
   $XDG_CACHE_HOME
)
for dir in "${dirs[@]}"
do
   mkdir -p $dir
done

# ensure XDG_BIN_HOME is in PATH
if [[ ":$PATH:" != *":$XDG_BIN_HOME:"* ]]; then
   PATH=$PATH:$XDG_BIN_HOME
fi

# set editor to neovim
export EDITOR=nvim
export VISUAL=nvim

# use less for paging
export PAGER=less
export LESS=-R

# use XDG dirs for asdf
export ASDF_DIR="$XDG_CONFIG_HOME/asdf"
export ASDF_CONFIG_FILE="$ASDF_DIR/.asdfrc"
export ASDF_DATA_DIR="$XDG_DATA_HOME/asdf"

