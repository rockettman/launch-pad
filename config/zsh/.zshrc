# .zshrc: executed by zsh for interactive shells
# vim: set ft=zsh:

# Set aliases
[ -f $ZDOTDIR/aliases ] && source $ZDOTDIR/aliases

# History settings
mkdir -p $XDG_STATE_HOME/zsh
HISTFILE=$XDG_STATE_HOME/zsh/histfile
HISTSIZE=10000
SAVEHIST=10000
setopt append_history           # allow multiple sessions to append to one history
setopt bang_hist                # treat ! special during command expansion
setopt extended_history         # Write history in :start:elasped;command format
setopt hist_expire_dups_first   # expire duplicates first when trimming history
setopt hist_find_no_dups        # When searching history, don't repeat
setopt hist_ignore_dups         # ignore duplicate entries of previous events
setopt hist_ignore_space        # prefix command with a space to skip it's recording
setopt hist_reduce_blanks       # Remove extra blanks from each command added to history
setopt hist_verify              # Don't execute immediately upon history expansion
setopt inc_append_history       # Write to history file immediately, not when shell quits

# Enable vi keybindings
set -o vi
bindkey -v

# Configure arrow history search
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
# Linux keys
bindkey "$key[Up]" up-line-or-beginning-search # Up
bindkey "$key[Down]" down-line-or-beginning-search # Down
# OSX keys
bindkey "^[[A" up-line-or-beginning-search # Up
bindkey "^[[B" down-line-or-beginning-search # Down

# >>> asdf >>> must be before compinit
if [ -s "$ASDF_DIR/asdf.sh" ]; then
    source $ASDF_DIR/asdf.sh
    fpath=(${ASDF_DIR}/completions $fpath)
fi
[ -s "$XDG_CONFIG_HOME/asdf-direnv/zshrc" ] && source "$XDG_CONFIG_HOME/asdf-direnv/zshrc"
# <<< asdf <<<

autoload -Uz compinit && compinit
autoload -Uz bashcompinit && bashcompinit

# Setup theme
#[ -f $XDG_DATA_HOME/zsh/themes/minimal.zsh ] && source $XDG_DATA_HOME/zsh/themes/minimal.zsh
if [ -f $XDG_DATA_HOME/zsh/themes/powerlevel10k/powerlevel10k.zsh-theme ]; then
    source $XDG_DATA_HOME/zsh/themes/powerlevel10k/powerlevel10k.zsh-theme
    # To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
    [ -f $ZDOTDIR/.p10k.zsh ] && source $ZDOTDIR/.p10k.zsh
fi

# cht.sh completions
[ -f $XDG_DATA_HOME/cht.sh/bash_completion ] && source $XDG_DATA_HOME/cht.sh/bash_completion

# add awscli completions
# complete -C '/usr/local/bin/aws_completer' aws
if which aws_completer > /dev/null; then
  complete -C aws_completer aws
fi

# >>> AWS SSO configuration >>>
export AWS_DEFAULT_SSO_START_URL=https://rockettman-labs.awsapps.com/start
export AWS_DEFAULT_SSO_REGION=us-east-1

alias sso='aws-sso-util'

_AWS_SSO_UTIL_COMPLETE_SCRIPT_DIR=$XDG_DATA_HOME/aws-sso-util
_AWS_SSO_UTIL_COMPLETE_SCRIPT=$_AWS_SSO_UTIL_COMPLETE_SCRIPT_DIR/complete.sh
if which aws-sso-util > /dev/null; then
  mkdir -p $_AWS_SSO_UTIL_COMPLETE_SCRIPT_DIR
  ({ _AWS_SSO_UTIL_COMPLETE=source_zsh aws-sso-util > $_AWS_SSO_UTIL_COMPLETE_SCRIPT.tmp ;
    mv $_AWS_SSO_UTIL_COMPLETE_SCRIPT.tmp $_AWS_SSO_UTIL_COMPLETE_SCRIPT; } &)
  if [ -f $_AWS_SSO_UTIL_COMPLETE_SCRIPT ]; then
    source $_AWS_SSO_UTIL_COMPLETE_SCRIPT
  fi
fi
# <<< AWS SSO configuration <<<

