# launch-pad
Environment configs just the way I like.

## Tools Configured

- git
- zsh
- alacritty
- tmux
- neovim
- tldr
- cht.sh
- asdf
    - direnv
    - python
    - nodejs
    - awscli


## Installation

Install Homebrew if using MacOS and use `brew` to install the rest of the dependencies.

Required dependencies:
- [GNU stow](https://www.gnu.org/software/stow/)
- [git](https://git-scm.com/)
- [wget](https://www.gnu.org/software/wget/)
- [curl](https://curl.se/)
- [zsh](https://zsh.sourceforge.io/)

Optional dependencies:
- [alacritty](https://alacritty.org/)
- [tmux](https://github.com/tmux/tmux/wiki)
- [neovim](https://neovim.io/)
- [ripgrep](https://github.com/BurntSushi/ripgrep)
- [fd](https://github.com/BurntSushi/ripgrep)

Setup tools and link configs:
```sh
git clone https://gitlab.com/rockettman/launchpad.git ~/.config/launchpad
~/.config/launchpad/install.sh
```

**NOTE:** Make sure dotfiles in your home dir are not preventing tools like tmux and git from searching the $XDG_CONFIG_HOME dir for the configs linked by this install.

Post install:
- Setup [asdf-direnv](https://github.com/asdf-community/asdf-direnv)
    - **NOTE:** Delete ~/.zshrc file created by setup otherwise it will block the zsh config installed by launchpad. The source line for asdf-direnv is already included in the zshrc installed by launchpad.
- Install asdf plugins
- Run `asdf direnv install` in your home dir to install user default versions of common system tools
- Run `direnv allow` in your home dir to authorize the .envrc file
- Use [asdf](https://asdf-vm.com/) for further global or local tool installs
- Install [Git Credential Manager](https://github.com/git-ecosystem/git-credential-manager)

